"use strict";

const {random} = require('./random');
const fs = require('fs');

exports.hide = (amount, itemList) => {
    const path = './field/';
    let numbers = [];
    let pokemons = [];

    if(amount > 3)
        amount = 3;

    for (let i = 0; i < amount; ++i) {
        let number = random(1, 10);
        let pokemon = itemList[random(1, 10)];
        if(numbers.length > 0)
            for (let j = 0; j < numbers.length; ++j) {
                if(numbers[j] === number && number !== 10)
                    number = number + 1;
                else if(numbers[j] === number && number === 10 && numbers[j] !== 0)
                    number = 0;
                else if(numbers[j] === number && number === 10 && numbers[j] === 0)
                    number = 1;
            }
        numbers.push(number);
        pokemons.push(pokemon);
    }

    fs.mkdir(path, (err) => {
        if(err && err.code !== 'EEXIST'){
        console.log(err);
        }
    });
    for (let i = 1; i <= 10; i++) {
        fs.mkdir(path + i, (err) => {
            if(!err || (err && err.code === 'EEXIST')){
            console.log('Folder %s created', i);
            } else {
                console.log(err);
            }
        });
    }
    for (let i = 0; i < numbers.length; i++) {
        fs.writeFile(path + numbers[i] + '/pokemon.txt', pokemons[i].name + '|' + pokemons[i].level, (writeErr) => {
            if (writeErr) throw writeErr;
            console.log('Pokemon is saved!');
        });
    }
    //console.log(numbers);
    //console.log(pokemons);
    return pokemons;
};

exports.seek = (path) => {
    fs.readdir(path, function(err, files) {
        if(err) return console.error(err);
        files.forEach(file => {
            if(fs.lstatSync(path + file).isFile()) {
                fs.readFile(path + file, { encoding: 'utf8' }, (err, content) => {
                    if(err) return console.error(err);
                console.log(path + ' => ' + content);
            });
            } else if(fs.lstatSync(path + file).isDirectory()) {
                let innerPath = path + file + '/';
                fs.readdir(innerPath, function(err, files) {
                    if(err) return console.error(err);
                    files.forEach(innerFile => {
                        fs.readFile(innerPath + innerFile, { encoding: 'utf8' }, (err, content) => {
                            if(err) return console.error(err);
                            console.log(innerPath + ' => ' + content);
                        });
                    });
                });
            }
        });
    });
};