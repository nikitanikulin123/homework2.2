"use strict";

exports.PokemonList = [
    {"name": "Bulbasaur", "level": 12},
    {"name": "Ivysaur", "level": 17},
    {"name": "Venusaur", "level": 22},
    {"name": "Charmander", "level": 51},
    {"name": "Bulbasaur2", "level": 212},
    {"name": "Ivysaur2", "level": 217},
    {"name": "Venusaur2", "level": 222},
    {"name": "Charmander2", "level": 251},
    {"name": "Bulbasaur3", "level": 312},
    {"name": "Ivysaur3", "level": 317},
    {"name": "Venusaur3", "level": 322},
    {"name": "Charmander3", "level": 351}
];